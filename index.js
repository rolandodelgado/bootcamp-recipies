const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user');
const recipeSchema = require('./models/recipe');

const app = express();

//Middleware
app.use(express.json())

//Mongoose
mongoose.connect('mongodb://poli:password@mongorecetas:27017/recipes?authSource=admin')
.then(()=>console.log("Conectando a Mongo"))
.catch((error)=> console.log(error))

//Routes
app.get('/',(req,res)=>{
    res.send('Hola mundo')
})

app.post('/new_user', (req,res) => {
    const user = userSchema(req.body)
    user.save()
    .then((data)=>res.json(data))
    .catch((error)=>res.send(error));
})

app.post('/new_recipe', (req,res) => {
    const recipe = recipeSchema(req.body)
    recipe.save() 
    .then((data)=>res.json(data))
    .catch((error)=>res.send(error))
})

app.post('/rate', (req,res) => {
    const {recipeId, userId, rating}=req.body
    recipeSchema.updateOne(
    {_id: recipeId},
    [
        {$set: { ratings: {$concatArrays: [{$ifNull: ['$ratings', []]}, [{ userId: userId, rating: rating}]]}} },
        {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}
    ]
    )
    .then((data)=>res.json(data))
    .catch((error)=>res.send(error))
})

app.get('/recipes', (req,res) => {
    const {userId, recipeId}=req.body
    if (recipeId){
        recipeSchema
            .find({_id : recipeId})
            .then((data) => res.json(data))
            .catch((error) => res.json({message : error}))
    } else {
        recipeSchema
        .find ({userId: userId})
        .then((data) => res.json(data))
        .catch((error) => res.json({message: error}))
    }
})

app.get('/recipesbyingredient', async (req, res) => {
    const { ingredients } = req.body;
    const query = recipeSchema.find({});

    for (const ingredient of ingredients) {
        query.where('ingredients.name').equals(ingredient.name.toLowerCase());
    }

    const recipes = await query.exec();

    res.json(recipes);
});

app.listen(3000, () => console.log("escuchando en puerto 3000"))